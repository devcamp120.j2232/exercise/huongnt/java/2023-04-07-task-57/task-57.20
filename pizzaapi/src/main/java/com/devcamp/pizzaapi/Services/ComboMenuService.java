package com.devcamp.pizzaapi.Services;

import java.util.ArrayList;

import javax.websocket.server.ServerEndpoint;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.Models.Menu;

@Service
public class ComboMenuService {

    //khởi tạo menu
    Menu menuSizeS = new Menu('S',"20cm",2,"200gr",2,150000.0);
    Menu menuSizeM = new Menu('M',"25cm",4,"300gr",3,200000.0);
    Menu menuSizeL = new Menu('L',"30cm",6,"300gr",5,250000.0);

    public ArrayList<Menu> getMenuCombo() {
        ArrayList<Menu> menuCombo = new ArrayList<>();
        menuCombo.add(menuSizeS);
        menuCombo.add(menuSizeM);
        menuCombo.add(menuSizeL);
        return menuCombo;
}
}