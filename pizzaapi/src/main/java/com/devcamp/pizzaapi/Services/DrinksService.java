package com.devcamp.pizzaapi.Services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizzaapi.Models.Drink;

@Service
public class DrinksService {
    Drink drink1 = new Drink ("TRATAC","Kumquat Tea",10000);
    Drink drink2 = new Drink ("TRASUA","Milk Tea",20000);
    Drink drink3 = new Drink ("COCA","Cococola",10000);
    Drink drink4 = new Drink ("PEPSI","Pepsi",12000);
    Drink drink5 = new Drink ("FANTA","Fanta",10000);
    Drink drink6 = new Drink ("LAVIE","Lavie",10000);

    public ArrayList<Drink> getAllDrinkMenu() {
        ArrayList<Drink> menuDrink = new ArrayList<>();
        menuDrink.add(drink1);
        menuDrink.add(drink2);
        menuDrink.add(drink3);
        menuDrink.add(drink4);
        menuDrink.add(drink5);
        menuDrink.add(drink6);
        return menuDrink;

}
}
