package com.devcamp.pizzaapi.Models;

public class Drink {
    public String maNuocUong;
    public String tenNuocUong;
    public double donGia;
    public String ghiChu = "Nothing";


    public Drink(String maNuocUong, String tenNuocUong, double donGia) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;

    }


    public String getMaNuocUong() {
        return maNuocUong;
    }


    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }


    public String getTenNuocUong() {
        return tenNuocUong;
    }


    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }


    public double getDonGia() {
        return donGia;
    }


    public void setDonGia(double donGia) {
        this.donGia = donGia;
    }


    public String getGhiChu() {
        return ghiChu;
    }


    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    


    
    
}
