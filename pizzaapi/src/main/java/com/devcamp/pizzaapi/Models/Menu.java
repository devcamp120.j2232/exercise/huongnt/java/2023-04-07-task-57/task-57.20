package com.devcamp.pizzaapi.Models;

public class Menu {
    public char combo;
    public String size;
    public int grilled;
    public String salad;
    public int Drink;
    public double price;
    
    public Menu(char combo, String size, int grilled, String salad, int drink, double price) {
        this.combo = combo;
        this.size = size;
        this.grilled = grilled;
        this.salad = salad;
        Drink = drink;
        this.price = price;
    }

    public char getCombo() {
        return combo;
    }

    public void setCombo(char combo) {
        this.combo = combo;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getGrilled() {
        return grilled;
    }

    public void setGrilled(int grilled) {
        this.grilled = grilled;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getDrink() {
        return Drink;
    }

    public void setDrink(int drink) {
        Drink = drink;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    
}
