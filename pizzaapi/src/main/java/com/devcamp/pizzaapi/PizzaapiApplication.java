package com.devcamp.pizzaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaapiApplication.class, args);
	}

}
