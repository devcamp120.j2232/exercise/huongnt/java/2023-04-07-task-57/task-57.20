package com.devcamp.pizzaapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.Models.Menu;
import com.devcamp.pizzaapi.Services.ComboMenuService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CComboMenuController {
    @Autowired
    ComboMenuService comboMenuService;

    @GetMapping("/comboS")
    public ArrayList<Menu> getMenuCombo(){
        return comboMenuService.getMenuCombo();
    }

}
