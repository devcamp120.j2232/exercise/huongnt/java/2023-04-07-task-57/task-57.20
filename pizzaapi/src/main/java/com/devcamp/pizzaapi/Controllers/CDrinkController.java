package com.devcamp.pizzaapi.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizzaapi.Models.Drink;
import com.devcamp.pizzaapi.Services.DrinksService;

@RestController
@CrossOrigin
@RequestMapping("")
public class CDrinkController {
    @Autowired
    DrinksService drinksService;
    @GetMapping("/drinks")

    public ArrayList<Drink> getMenuDrink(){
        System.out.println(drinksService.getAllDrinkMenu());
        return drinksService.getAllDrinkMenu();
        
    }

    
}
