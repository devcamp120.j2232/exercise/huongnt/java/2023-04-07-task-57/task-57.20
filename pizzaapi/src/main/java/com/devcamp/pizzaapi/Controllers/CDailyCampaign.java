package com.devcamp.pizzaapi.Controllers;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
@RestController
@CrossOrigin
@RequestMapping("/api")
public class CDailyCampaign {
    @GetMapping("/campaigns")
        public String getDate() {
            DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
            LocalDate today = LocalDate.now(ZoneId.systemDefault());
        
                return String .format("Hello pizza lover! Today is %s, buy one get one", dtfVietnam.format(today));
            }
    
}
